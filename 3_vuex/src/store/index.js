import Vue from 'vue';
import Vuex from 'vuex';
import check from './check';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    check
  },
  state: {
    root: false
  },
  mutations: {
    toggleRoot (state) {
      state.root = !state.root;
    },
    setRoot (state, payload) {
      state.root = payload;
    }
  },
  getters: {
    root: state => state.root
  }
});
