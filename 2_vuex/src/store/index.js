import Vue from 'vue';
import Vuex from 'vuex';
import check from './check';

Vue.use(Vuex);

const mutations = [
  'check/setCheck'
];

const myLogger = store => {
  store.subscribe(({ type, payload }, state) => {
    // if (mutations.includes(type)) {
    //   console.log(type);
    // }
    if (state.check.checked) {
      // console.log(type);
    }
  });

  store.subscribeAction(({ type, payload }, state) => {
    console.log(type);
  });
};

export default new Vuex.Store({
  plugins: [myLogger],
  modules: {
    check
  },
  state: {
    root: false
  },
  getters: {
    root: state => state.root
  }
});
