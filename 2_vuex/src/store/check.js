const state = {
  checked: false
};
const getters = {
  checked: state => state.checked
};
const mutations = {
  toggleCheck (state) {
    state.checked = !state.checked;
  },
  setCheck (state, payload) {
    state.checked = payload;
  }
};
const actions = {
  toggleCheck (context) {
    // console.log(context.rootState.check.checked);
    context.commit('toggleCheck');
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
