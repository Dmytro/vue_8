import Vue from 'vue';
import App from './App.vue';
import store from './store';
import config from './config';

Vue.config.productionTip = false;

console.log(config.url);

Vue.prototype.$url = config.url;

new Vue({
  config,
  store,
  render: h => h(App)
}).$mount('#app');
