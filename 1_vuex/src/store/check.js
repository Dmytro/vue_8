const state = {
  checked: false
};
const getters = {
  checked: state => state.checked
};
const mutations = {
  toggleCheck (state) {
    state.checked = !state.checked;
  }
};
const actions = {
  toggleCheck ({ commit, state }) {
    commit('toggleCheck');
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
